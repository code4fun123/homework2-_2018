#include <windows.h>
#include "stdio.h"
#include <iostream>
//#include "MyStaticLib.lib"

extern "C" void __cdecl dll1_method(void);

typedef void(__cdecl *OnStartProcedure)();

int main()
{

	dll1_method();

	
	HINSTANCE hModule2 = LoadLibrary(TEXT("MyDLL2.dll"));

	OnStartProcedure dll2Method = (OnStartProcedure)GetProcAddress(hModule2, "dll2_method");

	if (NULL == dll2Method)
		return -1;

	dll2Method();

	FreeLibrary(hModule2);

	//staticMethod();

	return 0;
}