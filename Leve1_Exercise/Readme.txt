	
- our solution is called "ConsoleApplication1" and contains four projects, from which we have: 
	* two dynamic libraries ("MyDLL1" and "MyDLL2") which were built immediately after implementing the functions from that libraries (with the "Build" command)
	* a static library ("MyStaticLib") in which a function called staticMethod() is implemented, and then we give the build command
	* an executable project ("Executable") where are called the functions from the libraries already built. Additionally, references to the "MyDLL1" dynamic library and to the
	  "MyStaticLib" static library were added to the executable project, to create the necessary links between projects.

- after the implementation of the executable program, this is built and is set to run, appearing in the console messages of the functions that have been called.