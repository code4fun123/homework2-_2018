#include "stdafx.h"
#include "BookReader.h"
#include<algorithm>

BookReader::BookReader()
{
}


BookReader::~BookReader()
{
}


 std::map<std::string, int>  BookReader::getWords()
{
	 return this->words;
}


void BookReader::readingBy1Word(std::string name)
{
	words.clear();
	std::ifstream file(name);

	std::string word;

	if (file.is_open())
	{
		while (!file.eof())
		{
			file >> word;
			
			if (words.find(word) != words.end())
			{
				words[word]++;
			}
			else
			{
				words[word] = 1;
			}
			
		}
	}
}

void BookReader::readingBy2Words(std::string name)
{
	words.clear();
	std::ifstream file(name);

	std::string word1,word2;

	if (file.is_open())
	{
		file >> word1 >> word2;
		while (!file.eof())
		{
			std::string expression = word1 + " " + word2;

			if (words.find(expression) != words.end())
			{
				words[expression]++;
			}
			else
			{
				words[expression] = 1;
			}
			word1 = word2;
			file >> word2;
		}
	}

}

void BookReader::readingBy3Words(std::string name)
{
	words.clear();
	std::ifstream file(name);

	std::string word1, word2, word3;

	if (file.is_open())
	{
		file >> word1 >> word2 >> word3;
		while (!file.eof())
		{
			std::string expression = word1 + " " + word2 + " " + word3;

			if (words.find(expression) != words.end())
			{
				words[expression]++;
			}
			else
			{
				words[expression] = 1;
			}

			word1 = word2;
			word2 = word3;
			file >> word3;
		}
	}

}



bool BookReader::checkExistence(std::string word)
{
	if (words.find(word) != words.end())
	{
		return true;
	}
	return false;
}


int BookReader::numberAppearances(std::string word)
{
	return words[word];
}


std::map < std::string, int>  BookReader::nCommonWords(int n)
{
	std::map < std::string, int> commonWord;

	std::map<std::string, int> aux = words;

	if (n <= words.size())
	{
		while (n > 0)
		{
			std::map<std::string, int>::iterator word = std::max_element(aux.begin(), aux.end(),
				[](const std::pair<std::string, int>& p1, const std::pair<std::string, int>& p2) {
				return p1.second < p2.second; });
			n--;
			commonWord[word->first] = word->second;
			aux.erase(word);
		}
	}
	
	return commonWord;
}


std::ostream& operator<<(std::ostream& f, const BookReader& bk )
{

	for (auto it : bk.words)
	{
		f << "( \"" << it.first << "\", appearances: " << it.second << ")\n\n";
	}
	return f;
}

