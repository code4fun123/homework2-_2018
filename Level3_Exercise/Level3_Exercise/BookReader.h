#pragma once

#include<iostream>
#include<map>
#include<string>
#include<fstream>
#include<vector>

class BookReader
{
private:

	std::map<std::string, int> words;

public:

	BookReader();
	~BookReader();

	std::map<std::string, int>  getWords();

	void readingBy1Word(std::string);

	void readingBy2Words(std::string);

	void readingBy3Words(std::string);

	bool checkExistence(std::string);

	int numberAppearances(std::string);

	std::map < std::string, int> nCommonWords(int);

	friend std::ostream& operator << (std::ostream&, const BookReader&);

};

