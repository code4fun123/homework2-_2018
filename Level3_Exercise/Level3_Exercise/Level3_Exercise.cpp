#include "stdafx.h"
#include "BookReader.h"
#include <Exception>
#include <vector>
#include <chrono>

using namespace std::chrono;

static void menu(int &choice)
{
	system("cls");
	std::cout << "\n1. Construct the container \n2. Check if a word exists.\n3. Count how many time a words appears.\n";
	std::cout<<"4. Print the top 3 most common words encountered\n5. The most common sequence of 2 words\n6. The most common sequence of 3 words\n7. Exit\n\nchoice: ";
	std::cin >> choice;
}

int main()
{
	int choice;
	BookReader * bk = NULL;

	high_resolution_clock::time_point start, stop;
	
	do
	{
		menu(choice);
		switch (choice)
		{
		case 1:
		{
			start = high_resolution_clock::now();

			bk = new BookReader();
			bk->readingBy1Word("book.txt");
			std::cout << "\nThe container was successfully built and it has " << bk->getWords().size() << " elements!\n\n";

			stop = high_resolution_clock::now();
			std::cout<< duration_cast<milliseconds>(stop - start).count() <<" miliseconds ...\n\n";


			system("pause");
			break;
		}
		case 2:

			if (bk == NULL)
			{
				std::cout << "\nPlease construct the container using first option.\n\n";
			}
			else
			{
				std::string word;
				std::cout << "\nInsert word: ";
				std::cin >> word;

				start = high_resolution_clock::now();

				if (bk->checkExistence(word))
				{
					std::cout << "\nThe word exists in container.\n\n";
				}
				else
				{
					std::cout << "\nThe word doesn't exist in container.\n\n";
				}

				stop = high_resolution_clock::now();
				std::cout << duration_cast<milliseconds>(stop - start).count() << " miliseconds ...\n\n";
			}
			system("pause");

			break;
		case 3:
			if (bk == NULL)
			{
				std::cout << "\nPlease construct the container using first option.\n\n";
			}
			else
			{
				start = high_resolution_clock::now();

				std::string word;
				std::cout << "\nInsert word: ";
				std::cin >> word;
				std::cout << "\nThe word appear " << bk->numberAppearances(word) << " times.\n\n";

				stop = high_resolution_clock::now();
				std::cout << duration_cast<milliseconds>(stop - start).count() << " miliseconds ...\n\n";
				
			}
			system("pause");
			break;
		case 4:
			if (bk == NULL)
			{
				std::cout << "\nPlease construct the container using first option.\n\n";
			}
			else
			{
				std::map < std::string, int> commonWords = bk->nCommonWords(3);
				std::cout << "\nThe most 3 words encountered: ";
				for (auto it : commonWords)
				{
					std::cout << "(" << it.first << "," << it.second << ")";
				}
				std::cout << "\n\n";

			}
			system("pause");
			break;
		case 5:
		{
			start = high_resolution_clock::now();

			bk = new BookReader();
			bk->readingBy2Words("book.txt");
			std::map < std::string, int> commonWords = bk->nCommonWords(1);

			stop = high_resolution_clock::now();

			std::cout << "\nThe most common sequence of 2 words: ";
			for (auto it : commonWords)
			{
				std::cout << "(" << it.first << "," << it.second << ")\n\n";
			}
			bk = NULL;
			std::cout << duration_cast<milliseconds>(stop - start).count() << " miliseconds ...\n\n";
			system("pause");
			break;
		}
		case 6:
		{
			start = high_resolution_clock::now();

			bk = new BookReader();
			bk->readingBy3Words("book.txt");
			std::map < std::string, int> commonWords = bk->nCommonWords(1);

			stop = high_resolution_clock::now();

			std::cout << "\nThe most common sequence of 3 words: ";
			for (auto it : commonWords)
			{
				std::cout << "(\"" << it.first << "\", " << it.second << ")\n\n";
			}
			bk = NULL;
			std::cout << duration_cast<milliseconds>(stop - start).count() << " miliseconds ...\n\n";
			system("pause");
			break;
		}
		case 7:
			std::cout << "\nHave a nice day!\n\n";
			break;
		default:
			break;
		}

	} while (choice != 7);

	if (bk != NULL)
	{
		delete bk;
	}

    return 0;
}


